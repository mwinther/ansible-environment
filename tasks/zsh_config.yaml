---
- name: Install zsh package
  ansible.builtin.package:
    name: zsh
    state: present
  become: true
  when: install_packages is true and install_zsh_packages is not false

- name: Create zsh config directory
  ansible.builtin.file:
    name: "{{ ansible_user_dir }}/.zsh"
    state: directory
    owner: "{{ ansible_user_id }}"
    group: "{{ user_group }}"
    mode: "0755"

- name: Create zsh config module directories
  ansible.builtin.file:
    name: "{{ ansible_user_dir }}/.zsh/{{ item }}"
    state: directory
    owner: "{{ ansible_user_id }}"
    group: "{{ user_group }}"
    mode: "0755"
  loop:
    - zshrc
    - zshenv

- name: Create zsh config module scripts
  ansible.builtin.copy:
    src: "files/zsh-modules/{{ item }}"
    dest: "{{ ansible_user_dir }}/.{{ item }}"
    remote_src: true
    owner: "{{ ansible_user_id }}"
    group: "{{ user_group }}"
    mode: "0644"
  loop:
    - zshrc
    - zshenv

- name: Copy modules into zshenv module directory
  ansible.builtin.copy:
    src: "files/zsh-modules/{{ item }}"
    dest: "{{ ansible_user_dir ~ '/.zsh/zshenv/' ~ item }}"
    force: "{{ zsh_config_force_overwrite }}"
    owner: "{{ ansible_user_id }}"
    group: "{{ user_group }}"
    mode: "0644"
  loop: "{{ zsh_zshenv_modules }}"

- name: Copy modules into zshrc module directory
  ansible.builtin.copy:
    src: "files/zsh-modules/{{ item }}"
    dest: "{{ ansible_user_dir ~ '/.zsh/zshrc/' ~ item }}"
    force: "{{ zsh_config_force_overwrite }}"
    owner: "{{ ansible_user_id }}"
    group: "{{ user_group }}"
    mode: "0644"
  loop: "{{ zsh_zshrc_modules }}"

- name: Set fact for alias location
  ansible.builtin.set_fact:
    zsh_alias_location: "{{ ansible_user_dir ~ '/.zsh/zshenv/alias' }}"
  when: "'alias' in zsh_zshenv_modules"

- name: Set fact for alias location
  ansible.builtin.set_fact:
    zsh_alias_location: "{{ ansible_user_dir ~ '/.zsh/zshrc/alias' }}"
  when: "'alias' in zsh_zshrc_modules"

- name: Add alias to always run package manager as sudo
  ansible.builtin.set_fact:
    zsh_dynamic_aliases: >
      {{ zsh_dynamic_aliases | d({}) | combine(_sudo_pkgmgr) }}
  vars:
    _sudo_pkgmgr:
      dnf: '''sudo -A dnf'''
      yum: '''sudo -A yum'''
  when: ansible_os_family == "RedHat"

- name: Combine static and dynamic aliases
  ansible.builtin.set_fact:
    _zsh_aliases: >
      {{ zsh_static_aliases | d({}) | combine(zsh_dynamic_aliases) }}

- name: Prepare alias block
  ansible.builtin.set_fact:
    _alias_block: >
      {{ _alias_block | d('') ~
      'alias ' ~ item.key ~ '=' ~ item.value }}
  loop: "{{ _zsh_aliases | dict2items }}"

- name: Add dynamic aliases to zsh module
  ansible.builtin.blockinfile:
    path: "{{ zsh_alias_location }}"
    marker: "# {mark} ANSIBLE ALIASES"
    block: "{{ _alias_block }}"

- name: Add static environment variables to zshenv
  ansible.builtin.template:
    src: templates/environment_variables.j2
    dest: "{{ ansible_user_dir ~ '/.zsh/zshenv/ansible_environment' }}"
    owner: "{{ ansible_user_id }}"
    group: "{{ user_group }}"
    mode: "0644"
